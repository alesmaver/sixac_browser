apt-get update
apt-get install git
apt-get install wget
apt-get install screen
apt-get install python-pip
apt-get install nano
apt-get install perl
apt-get install unzip
apt-get install r-base
apt-get install libssl-dev openssl libcurl4-openssl-dev
apt-get install libmariadb-client-lgpl-dev
apt-get install libxml2-dev
apt-get install vcftools

mkdir exac
cd exac
wget http://broadinstitute.org/~konradk/exac_browser/exac_browser.tar.gz .
tar zxvf exac_browser.tar.gz
git clone https://alesmaver@bitbucket.org/alesmaver/sixac_browser.git

apt-get install mongodb
mkdir database
screen -dmS mongod bash -c 'mongod --dbpath ./database'

cd exac_browser
pip install -r requirements.txt
pip install Flask-Runner
python manage.py load_db

cd ..
git clone https://github.com/Ensembl/ensembl-vep.git
cd ensembl-vep
apt-get install libdbd-mysql-perl
perl INSTALL.pl --NO_HTSLIB # One needs to interact with the installation during this step 

cd ..
git clone https://github.com/Ensembl/VEP_plugins.git
cp VEP_plugins/LoFtool_scores.txt ~/.vep/Plugins/

mkdir VCF